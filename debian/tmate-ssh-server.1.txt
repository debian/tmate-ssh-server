NAME
  tmate-ssh-server - server side part of tmate

SYNOPSIS
  tmate-ssh-server [-b ip] [-h hostname] [-k keys_dir] [-a authorized_keys_path] [-p port] [-x proxy_hostname] [-q proxy_port] [-s] [-v]

DESCRIPTION
  tmate-ssh-server is the server side part of tmate. It acts as a ssh-server and takes connections from tmate clients.

OPTIONS
  -b *ip* to bind to.

  -h *hostname*.

  -k *keysdir* directory containing the ssh-keys.

  -a *authorized_keys_path* path to authorized keys file.

  -p *port* to listen on.

  -x *proxy_hostname*.

  -q *proxy_port*.

  -s use syslog.

  -v verbose output.

SEE ALSO
  tmate(1)
